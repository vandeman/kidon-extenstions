﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KidonExtensions.Strings.Conversion
{
    public static class StringNumberMethods
    {
        /// <summary>
        /// Converts an string (e.g. "1234") to a 32-bit integer. Throws exception if string is out of 32-bit integer range
        /// </summary>
        /// <param name="s"></param>
        /// <returns></returns>
        public static int IntValue(this string s)
        {
            return Convert.ToInt32(s);
        }

        public static int IntegerValue(this string s)
        {
            return IntValue(s);
        }

        public static Int64 Int64Value(this string s)
        {
            return Convert.ToInt64(s);
        }

    }
}
