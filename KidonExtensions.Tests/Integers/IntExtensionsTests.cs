﻿using KidonExtenstions.Integers;
using NUnit.Framework;

namespace KidonExtensions.Tests.Integers
{
    [TestFixture]
    public class IntExtensionsTests
    {


        [Test]
        [TestCase(2)]
        [TestCase(3)]
        [TestCase(5)]
        [TestCase(7)]
        [TestCase(11)]
        [TestCase(13)]
        [TestCase(17)]
        [TestCase(19)]
        [TestCase(23)]
        [TestCase(29)]
        [TestCase(31)]
        [TestCase(877)]
        [TestCase(991)]
        public void IsPrimeTests(int x)
        {
            Assert.IsTrue(x.IsPrime());
        }

        [Test]
        [TestCase(1)]
        [TestCase(6)]
        [TestCase(15)]
        [TestCase(35)]
        [TestCase(100)]
        [TestCase(864)]
        public void IsNotPrime(int x)
        {
            Assert.IsFalse(x.IsPrime());
        }

    }
}
