﻿using System;
using KidonExtensions.Strings.Validation;
using NUnit.Framework;

namespace KidonExtensions.Tests.Strings.Validation
{
    [TestFixture]
   public class StringFormValidationExtensionsTests
    {

        [Test]
        [TestCase("", false)]
        [TestCase("ghysd@jtc.ie", true)]
        [TestCase("david.jones@proseware.com", true)]
        [TestCase("d.j@server1.proseware.com", true)]
        [TestCase("j.@server1.proseware.com", false)]
        [TestCase("js#internal@proseware.com", true)]
        [TestCase("j_9@[129.126.118.1]", true)]
        [TestCase("js*@proseware.com", false)]
        public void IsEmailTests(string s, bool expectedIsEmail)
        {
            
            //Act
            var isEmail = s.IsEmail();

            //Assert
            Assert.AreEqual(expectedIsEmail, isEmail);

        }

    }
}
