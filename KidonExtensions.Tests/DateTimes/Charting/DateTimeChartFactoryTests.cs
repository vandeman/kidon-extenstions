﻿using KidonExtensions.DateTimes.Charting;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KidonExtensions.Tests.DateTimes.Charting
{
    [TestFixture]
    public class DateTimeChartFactoryTests
    {

        [Test]
        public void CreateTenPointsOverTenMinuteTimespan()
        {
            //Arrange
            var startDate = new DateTime(2014, 7, 26, 9, 0, 0);
            var endDate = new DateTime(2014, 7, 26, 9, 10, 0);

            //Act
            var dateRange = DateTimeChartFactory.CreateDateRange(startDate, endDate, TimeSpan.FromMinutes(1));

            //Assert
            Assert.AreEqual(10, dateRange.Count());
            Assert.AreNotEqual(endDate, dateRange[9]); //End time must not be the last time!!!
            for(int i = 0; i< 10;i++)
            {
                Assert.AreEqual(startDate.AddMinutes(i), dateRange[i]);
            }



        }

    }
}
