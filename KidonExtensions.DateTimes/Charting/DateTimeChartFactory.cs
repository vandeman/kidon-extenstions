﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KidonExtensions.DateTimes.Charting
{
    public class DateTimeChartFactory
    {
        /// <summary>
        /// Create an array of dateTimes between two points in time
        /// </summary>
        /// <param name="fromDate"></param>
        /// <param name="toDate">End time (not inclusive)</param>
        /// <param name="interval"></param>
        /// <returns></returns>
        public static DateTime[] CreateDateRange(DateTime fromDate, DateTime toDate, TimeSpan interval)
        {
            var dateList = new List<DateTime>();
            var cursor = fromDate;
            while (cursor < toDate)
            {
                dateList.Add(cursor);
                cursor += interval;
            }
            return dateList.ToArray();
        }

    }
}
