﻿using System;

namespace KidonExtenstions.Integers
{
    public static class IntExtensions
    {
        /// <summary>
        /// A prime number (or a prime) is a natural number greater than 1 that has no positive divisors other than 1 and itself.
        /// </summary>
        /// <param name="x"></param>
        /// <returns></returns>
        public static bool IsPrime(this int x)
        {
            var boundary = Math.Floor(Math.Sqrt(x));

            if (x == 1) return false;
            if (x == 2) return true;

            for (var i = 2; i <= boundary; ++i)
            {
                if (x % i == 0) return false;
            }

            return true;     
        }

    }
}
