﻿using System.Collections.Generic;
using System.Linq;


namespace KidonExtensions.Arrays
{
    public class IEnumerableFactory
    {

        public static IEnumerable<T> CreateEnumerable<T>(T defaultValue, int size)
        {
            var enumerable = new List<T>(size);
           
            for (var i = 0; i < size; i++)
            {
                enumerable.Add(defaultValue);
            }
            return enumerable;
        }

    }
}
